(function () {

const $ = sel => document.querySelector(sel);
const $$ = sel => document.querySelectorAll(sel);

const routes = [];
const route = (url, cb) => routes.push({ url, cb });

// trigger a callback if given url matches current url
const go = () => {
	const loc = (location.pathname + location.hash).replace('#', '/');
	for (const { url, cb } of routes) {
		const params = [];
		const re = '^' + url.split(/[/#]/).map(p => {
			if (p.startsWith(':')) {
				params.push(p.slice(1));
				return '([^/]+)';
			} else {
				return p;
			}
		}).join('/') + '$';
		const match = loc.match(new RegExp(re));
		if (!match) continue;
		const obj = {};
		for (let i = 0; i < params.length; i++) {
			obj[params[i]] = match[i + 1];
		}
		return cb(obj);
	}
};

setTimeout(go); // wait until dom render
window.addEventListener('hashchange', go);

const searchRepo = async (user, repo, regexp) => {
	const res = await fetch(`/api/v1/repos/${user}/${repo}/contents`);
	const file = (await res.json()).find(f => f.path.match(regexp));
	if (!file) return null;
	const contents = await (await fetch(file.download_url)).text();
	return {
		name: file.name,
		path: file.path,
		contents
	};
};

const parseNTriples = txt => {
	const lines = txt.split(/[\n\r]+/);
	const triples = [];

	const basicstring = '"(?:[^"]*\\")*[^"]*'; // allow escaped strings (\")
	const string = basicstring + "(?:^^<[^>]+>|@\\w+)?"; // "blah"@en_US "thing"^^<http://...>
	const iri = '<[^>]+>'; // <http://...>
	const blank = '_:\\w+'; // _:34322
	const term = '(' + string + '|' + iri + '|' + blank + ')';
	const ws = '\\s+';
	const end = '\\.\\s*';
	const comment = '(?:#(.*))?'; // # blah
	const triple = '^' + term + ws + term + ws + term + ws + end + comment + '$';
	for (const line of lines) {
		if (line.trim() === '') continue;
		let match = line.match(new RegExp(triple));
		if (!match) {
			console.error('error parsing NTriples line', line);
			continue;
		}
		triples.push([match[1], match[2], match[3], match[4]]); // last element is comment
	}
	return triples;
};

const findDCAT = async (user, repo) => {
	const file = await searchRepo(user, repo, /.nt$/);
	if (!file) return null;
	console.log(file);
	return { path: file.path, triples: parseNTriples(file.contents) };
};

const fetchingMappings = fetch('/mappings.en.json').then(m => m.json());

const parseTerm = async term => {
	if (term.startsWith('<')) { // IRI
		const mappings = await fetchingMappings;
		return { value: mappings.OBJ[term] || term };
	}
	if (term.startsWith('"')) { // stirng
		const i = term.lastIndexOf('"@');
		let lang;
		if (i > -1) lang = term.slice(i + 2);
		return {
			value: JSON.parse(term.slice(0, term.lastIndexOf('"') + 1)), // JSON parse handles escaped unicode, newlines, etc
			lang
		};
	}
	return { value: term };
};

const findMetadata = async (user, repo) => {
	const dcat = await findDCAT(user, repo);
	if (!dcat) return null;
	const mappings = await fetchingMappings;
	const metadata = {};
	for (const [term, text] of Object.entries(mappings.REL)) {
		const matches = dcat.triples.filter(triple => triple[1] === term);
		console.log(term, matches, text);
		for (const match of matches) {
			if (!metadata[text]) metadata[text] = [];
			metadata[text].push(await parseTerm(match[2]));
		}
	}
	return metadata;
};



// const testCSV = `product,category,cost,purchases
// toaster,appliances,15.00,902
// fridge,appliances,200.00,140
// washing machine,appliances,359.99,120
// table,furniture,149.99,144
// chair,furniture,49.49,284
// sofa,furniture,349.99,2
// plant pot,garden,6.79,672
// bbq,garden,212.00,39
// `;

// const testRDF = `
// `;

const customContainer = () => {
	$('.repository.file.list > .ui.container:not(#extra)').classList.add('code-listing');
	let $container = $('#extra');
	if (!$container) {
		$container = document.createElement('div');
		$container.id = 'extra';
		$container.classList.add('ui', 'container');
		$('.repository.file.list').appendChild($container);
	}
	$container.innerHTML = '';
	return $container;
};

const activateTab = endOfUrl => {
	for (const $item of $$('.repository .navbar .item')) {
		$item.classList.toggle('active', $item.href.endsWith(endOfUrl));
	}
};

route('/:user/:repo', async () => {
	location.hash = '#about';
});

route('/:user/:repo#about', async ({ user, repo }) => {
	$('.repository.file.list').setAttribute('x-page', 'about');
	activateTab(`/${user}/${repo}#about`);
	const $container = customContainer();
	const metadata = await findMetadata(user, repo);
	$container.innerHTML = '<h2>About this dataset</h2>';
	if (metadata) {
		const $tbl = document.createElement('table');
		$tbl.classList.add('dcat-metadata');
		const $thead = document.createElement('thead');
		$thead.innerHTML = `<tr><th>Property</th><th>Lang</th><th>Values</th></tr>`;
		const $tbody = document.createElement('tbody');
		$tbl.append($thead);
		$tbl.append($tbody);
		for (const [prop, values] of Object.entries(metadata)) {
			let $row = document.createElement('tr');
			const $prop = document.createElement('th');
			$prop.setAttribute('rowspan', values.length);
			$prop.textContent = prop;
			$row.append($prop);
			$tbody.append($row);
			for (let i = 0; i < values.length; i++) {
				if (i > 0) {
					$row = document.createElement('tr');
					$tbody.append($row);
				}
				const $lang = document.createElement('td');
				const $val = document.createElement('td');
				$val.classList.add('pre');
				$lang.textContent = values[i].lang || '';
				$val.textContent = values[i].value;
				$row.append($lang);
				$row.append($val);
			}
		}
		$container.append($tbl);
	} else {
		const $msg = document.createElement('div');
		$msg.innerHTML = '<p>No metadata for this dataset.</p><p><a href="#dcat/edit">Create metadata</a></p>';
		$container.append($msg);
	}
});

route('/:user/:repo#code', async ({ user, repo }) => {
	$('.repository.file.list').setAttribute('x-page', 'code');
	activateTab(`/${user}/${repo}#code`);
	customContainer(); // empty the custom container
});

route('/:user/:repo#dcat/:mode', async ({ user, repo, mode }) => {
	$('.repository.file.list').setAttribute('x-page', 'dcat');
	activateTab(`/${user}/${repo}#dcat/show`);
	const $container = customContainer();
	const $header = document.createElement('h2');
	$header.classList.add('ui', 'header');
	$header.textContent = 'Catalog Metadata';
	$container.append($header);
	const $toolbox = document.createElement('div');
	$toolbox.classList.add('ui', 'right');
	$header.appendChild($toolbox);

	const $tbody = document.createElement('tbody');
	
	const dcat = await findDCAT(user, repo);

	if (mode === 'show') {
		const $edit = document.createElement('a');
		$edit.classList.add('ui', 'small', 'green', 'button');
		$edit.textContent = 'Edit';
		$edit.href = `/${user}/${repo}#dcat/edit`;
		$toolbox.append($edit);
	} else {
		const $save = document.createElement('a');
		$save.classList.add('ui', 'small', 'green', 'button');
		$save.textContent = 'Save';
		//$save.href = `/${user}/${repo}#dcat/show`;
		$save.addEventListener('click', async e => {
			let newdcat = [];
			for (const $row of $tbody.querySelectorAll('tr')) {
				const triple = [];

				for (const $inp of $row.querySelectorAll('textarea')) {
					triple.push($inp.value);
				}
				newdcat.push(`${triple[0]} ${triple[1]} ${triple[2]} .` + (triple[3] ? ` # ${triple[3]}` : ''));
			}

			alert('TODO: this should create a pull request with the new DCAT file');
			// fork
			const method = dcat ? 'PUT' : 'POST';
			const path = dcat ? dcat.path : 'main.dcat';

			let fileRes = await fetch(`/api/v1/repos/${user}/${repo}/contents/${path}`, {
				method,
				headers: {
					'content-type': 'text/json'
				},
				body: JSON.stringify({
					author: {
						email: 'example@gmail.com',
						name: 'Example'
					},
					new_branch: 'test-' + Math.round(Math.random() * 100000000),
					committer: {
						email: 'example@gmail.com',
						name: 'Example'
					},
					dates: {
						author: "2020-01-05T21:46:28.849Z",
						committer: "2020-01-05T21:46:28.849Z"
					},
					content: newdcat.join('\n'),
					message: `Update DCAT`
				})
			});

			const d = await fileRes.json();
			console.log(d);

			//post `/repos/${user}/${repo}/pulls` fileRes.commit.sha

			//const url = await pullRequest(user, repo, dcatPath, newdcat.join('\n'));
			//location.href = '';
		});
		$toolbox.append($save);
	}

	if (dcat) {
		const $tbl = document.createElement('table');
		const $thead = document.createElement('thead');
		$tbl.append($thead);
		$tbl.append($tbody);
		$tbl.classList.add('dcat');
		$thead.innerHTML = `<tr><th>Subject</th><th>Predicate</th><th>Object</th><th>Comment</th></tr>`;
		for (const triple of dcat.triples) {
			const $row = document.createElement('tr');
			for (const val of triple) { // there is an additional "comment" field in the array
				const $cel = document.createElement('td');
				if (mode === 'edit') {
					const $inp = document.createElement('textarea');
					$inp.value = val === undefined ? '' : val;
					$cel.appendChild($inp);
				} else {
					$cel.textContent = val === undefined ? '' : val;
				}
				$row.append($cel);
			}
			$tbody.append($row);
		}
		$container.append($tbl);
		if (mode === 'edit') {
			const $add = document.createElement('a');
			$add.classList.add('ui', 'small', 'green', 'button');
			$add.textContent = 'Add row';
			$add.addEventListener('click', () => {
				const $row = document.createElement('tr');
				$tbody.append($row);
				for (let i = 0; i < 4; i++) {
					const $cel = document.createElement('td');
					$row.append($cel);
					const $inp = document.createElement('textarea');
					$cel.append($inp);
				}
			});
			$container.append($add);
		}
	} else {
		const $msg = document.createElement('div');
		$msg.innerHTML = '<p>No metadata for this dataset.</p><p><a href="#dcat/edit">Create metadata</a></p>';
		$container.append($msg);
	}
});


const csvstringify = (val) => {
	if (typeof val === 'string') {
		val = val.replace(/[\n\r]+/g, ' ').replace(/"/g, '""');
		if (val.includes(',') || val.includes('"')) {
			val = `"${val}"`;
		}
	} else if (val instanceof Date) {
		val = val.toISOString();
	} else if (typeof val === 'undefined' || val === null || isNaN(val)) {
		val = '';
	}
	return val;
};


/*
Internal representation of dataset is JSON:
[
	{ product: 'toaster', category: 'appliance', price: 20 }
]
*/

const importers = {
	csv: text => {
		const lines = text.trim().split('\n');
		let headers;
		const rows = [];
		for (const line of lines) {
			const row = [];
			const chars = line.split('');
			let cell = '';
			let prev, instring;
			for (const c of chars) {
				if (c === '\u0000') continue;
				if (c === '"') {
					instring = !instring;
					if (prev === '"') cell += '"';
				} else if ((c === ',' || c === ';') && !instring) {
					row.push(cell);
					cell = '';
				} else {
					cell += c;
				}
				prev = c;
			}
			row.push(cell);
			if (!headers) {
				headers = row;
			} else {
				let obj = {};
				for (const header of headers) {
					obj[header.trim()] = row.shift();
				}
				rows.push(obj);
			}
		}
		return rows;
	},
	rdf: () => {},
	json: text => JSON.parse(text),
	xml: () => {}
};

const exporters = {
	csv: dataset => {
		const headers = Object.keys(dataset[0]);
		return [
			headers.join(','),
			...dataset.map(t => headers.map(h => csvstringify(t[h])).join(','))
		].join('\n');
	},
	rdf: () => {},
	json: dataset => JSON.stringify(dataset, null, '\t'),
	xml: dataset => {
		const headers = Object.keys(dataset[0]);
		return `<?xml version="1.0"?>
		<dataset>
			${dataset.map(t => `
			<item>${headers.map(h => `
				<${h}>${t[h]}</${h}>`).join('')}
			</item>`).join('')}
		</dataset>`;
	}
};

const getDataset = async (user, repo) => {
	for (const [type, importer] of Object.entries(importers)) {
		const file = await searchRepo(user, repo, new RegExp(`\\.${type}$`));
		if (file) {
			const dataset = importer(file.contents);
			if (!(dataset instanceof Array)) {
				console.error('dataset is not an array', file);
				continue;
			}
			return dataset;
		}
	}
};

route('/:user/:repo/releases', async ({ user, repo }) => {
	activateTab(`/${user}/${repo}/releases`);
	for (const [type, exporter] of Object.entries(exporters)) {
		const $download = document.createElement('a');
		$download.classList.add('ui', 'small', 'green', 'button');
		$download.textContent = `Download ${type.toUpperCase()}`;
		$download.addEventListener('click', async () => {
			const dataset = await getDataset(user, repo);
			const data = exporter(dataset);
			const blob = new Blob([data], {type: "octet/stream"});
			const url = window.URL.createObjectURL(blob);
			const a = document.createElement("a");
			document.body.appendChild(a);
			a.style = "display: none";
			a.href = url;
			a.download = `${repo}.${type}`;
			a.click();
			document.body.removeChild(a);
			window.URL.revokeObjectURL(url);
		});
		$('#release-list').parentNode.insertBefore($download, $('#release-list'));
	}
});

}());